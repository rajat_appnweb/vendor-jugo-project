import { Type } from "class-transformer";
import { BaseComponent } from "../../framework/BaseCompo";
import { BaseResponse } from "../../framework/BaseResponseModel";


export class Login {
    username: string;
    password: string;


    public get userNameError() {
        if (this.username) {
            return true;
        }
       return false;
    }


    public get passwordError() {
        if (this.password) {
            return true;
        }
        return false;
    }

    public get isValid(){
        if (this.userNameError || this.passwordError) {
            return true;
        }
        return false;
    }

}



export class LoginResponse extends BaseResponse{
    @Type(() => Vendor)
    data: Vendor;
}



export class Vendor {
    @Type(() => Login)
    login: Login;
    created: string;
    updated: string;
    status: string;
    id: number;
    role: string;
    email: string;
    token: string;
    lat: number;
    lng: number;
    address: string;
    firstName: string;
    lastName: string;
    username: string;
    contactNumber: string;
    alternateContact: string;
    businessName: string;
    displayCount: number;
    profileViewCount: number;
    emailContactCount: number;
    phoneContactCount: number;
    categoryList?: any;
    aboutText?: any;
    tagline?: any;
    averageRating?: any;
    defaultRating: number;
    photo?: any;
    paymentPending: boolean;



    public get getFullName() {
        return this.firstName + ' '  + this.lastName;
    }
}

export class ForgotPassword {
    emailId: string;
}
export class ForgotPasswordResponse extends BaseResponse {
    @Type(() => ForgotPassword)
    data: ForgotPassword;
  }

