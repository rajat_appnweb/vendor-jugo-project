import { Type } from 'class-transformer';
import { BaseResponse } from './../framework/BaseResponseModel';

export class PaymentRequest {
    purpose: string;
    amount: string;
    email: string;
    phone: string;
    webhook: string;
    id: string;
    status: string;
    buyer_name: string;
    redirect_url: string;
    send_email: boolean;
    send_sms: boolean;
    allow_repeated_payments: boolean;
    mark_fulfilled?: any;
    expires_at?: any;
    sms_status: string;
    email_status: string;
    shorturl?: any;
    longurl: string;
    customer_id?: any;
    created_at: Date;
    modified_at: Date;
}

export class PaymentDetail {
    success: boolean;
    @Type(() => PaymentRequest)
    payment_request: PaymentRequest;
}

export class PaymentResponse extends BaseResponse {
    @Type(() => PaymentDetail)
    data: PaymentDetail;
}
