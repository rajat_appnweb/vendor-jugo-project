import { BaseResponse } from "../../framework/BaseResponseModel";
import { Type } from "class-transformer";
import { DateUtil } from "../../framework/Utils/DateUtil";


export class User {
    firstName: string;
    lastName: string;
    username: string;
    profileImgUrl: string;
    phoneNumber: string;
    userId: number;
    referralPartner: boolean;
    referralCode?: any;

}

export class UserCount {
    totalcount: number;
    @Type(() => User)
    user: User;
    id: number;
    countType: string;
    vendorId: number;
    created: string;
    updated: string;
    localDateTime: string;

    public get getTime() {
        console.log("get local time::")
        if (this.created) {
            return DateUtil.getLocalDatefromServer(this.created, "DD-MM-YYYY hh:mm:ss", "DD-MM-YYYY hh:mm a");
        }
        return "";
    }
}

export class CountResponse extends BaseResponse {
    @Type(() => UserCount)
    data: UserCount[];
    @Type(() => Pagination)
    pagination: Pagination;
}


export class CountType {
    id: number;
    countType: string;
    pageNo: number;
}

export class Pagination {
    from: number;
    path: string;
    to: number;
    total: number;
    current_page: number;
    first_page_url: string;
    last_page: number;
    last_page_url: string;
    next_page_url: string;
    per_page: number;
}


export enum CountTypeEnum {
    PROFILE_VIEW_COUNT = 'PROFILE_VIEW_COUNT',
    PHONE_CONTACT_COUNT = 'PHONE_CONTACT_COUNT',
    EMAIL_CONTACT_COUNT = 'EMAIL_CONTACT_COUNT',
    DISPLAY_COUNT = 'DISPLAY_COUNT'

}



