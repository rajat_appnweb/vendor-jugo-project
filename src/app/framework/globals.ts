// import { DashboardComponent } from './../layout/dashboard/dashboard.component';

// export const API_BASE_URL = 'http://52.14.184.135:8080';

// export const API_BASE_URL = 'https://api.jugo.co.in';

export const API_BASE_URL = 'http://192.168.0.4:8080';
// export const API_BASE_URL = 'http://1d76549f.ngrok.io';

export const VENDOR_APP = API_BASE_URL + '/jugoapi';
export const URL_USER = VENDOR_APP + '/vendor';
export const SIGN_UP = URL_USER + '/createVendor';
export const LOGIN = URL_USER + '/login';
export const USER = URL_USER + '/';
export const GET_ALL_VENDOR = URL_USER;
export const UPDATE_PROFILE = URL_USER + '/updateVendor';
export const UPLOAD_PHOTO = VENDOR_APP + '/fileUpload';
export const UPLOAD_IMAGE = URL_USER + '/vendorProfile';
export const UPDATE_STATUS = URL_USER + "/";
export const GET_COUNTTYPE = URL_USER + "/vendorCountType/";
export const GET_ALL_CITY = VENDOR_APP + "/city/all";
export const ADD_CITY = VENDOR_APP + "/city/add";
export const UPDATE_CITY = VENDOR_APP + "/city/update";
export const GET_ALL_CATEGORY = VENDOR_APP + "/category";
export const ADD_CATEGORY = VENDOR_APP + "/vendorCategory/create";
export const UPDATE_CATEGORY = VENDOR_APP + "/category/";
export const DELETE_CATEGORY = UPDATE_CATEGORY;
export const ADD_ALL_CATEGORY = VENDOR_APP + "/category";
export const GET_ALL_CATEGORY_BY_VENDOR_ID = VENDOR_APP + "/category";
export const GET_SUBSCRIPTION = VENDOR_APP + "/subscriptions";
export const ADD_ALL_PACKAGE = VENDOR_APP + "/paymentPackages";
export const UPDATE_PACKAGE = VENDOR_APP + "/paymentPackages";

export const DELETE_PACKAGE = VENDOR_APP + "/paymentPackages/";
export const MAKE_PAYMENT = VENDOR_APP + "/payment";
export const GET_MAX_VALUE_VENDOR_SUBSCRIPTION = VENDOR_APP + "/paymentPackages/getPaymentAmount";
export const GET_PAYMENT_HISTORY = VENDOR_APP + "/payment/transactionhistory";
export const CONFIRM_PENDING_PAYMENT = VENDOR_APP + "/payment/";
export const REJECT_PENDING_PAYMENT = VENDOR_APP + "/payment/rejectPayment/";
export const DELETE_VENDOR_IMAGE = URL_USER + '/vendorProfile/';
export const RESET_PASSWORD = URL_USER + '/chnagePassword';
export const FORGOT_PASSWORD = URL_USER + '/forgotPassword';

export const VERIFY_ONLINE_PAYMENT = VENDOR_APP + '/payment/transactionNumber';
export const UPDATE_CATEGORY_STATUS = VENDOR_APP + '/category/updateActiveStatus';

export const UPDATE_CITY_STATUS = VENDOR_APP + '/city/updateActiveStatus';
export const CREATE_EVENT = VENDOR_APP+'/event/create';
//export const GET_DESIGN = VENDOR_APP+'/design/?eventTemplateId=';
export const GET_ALL_EVENT_TEMPLATE = VENDOR_APP+'/eventTemplates/';
export const GET_EVENT_BY_ID = VENDOR_APP+'/design/?eventTemplateId=';
export const ADD_IMAGE = VENDOR_APP+'/fileUpload';
export const SAVE_IMAGE =VENDOR_APP+'/design/create';
export const DELETE_DESIGN_IMAGE = VENDOR_APP+'/design/delete/';
export const DELETE_EVENT_TEMPLATE = VENDOR_APP+'/eventTemplates/delete?templateId=';
export const EDIT_IMAGE = VENDOR_APP+'/design/update'


export enum TaskCode {
    SIGN_UP,
    LOGIN,
    GET_ALL_VENDOR,
    USER,
    UPDATE_PROFILE,
    UPLOAD_PHOTO,
    UPLOAD_IMAGE,
    UPDATE_STATUS,
    GET_COUNTTYPE,
    ADD_CATEGORY,
    GET_ALL_CATEGORY,
    UPDATE_CATEGORY,
    DELETE_CATEGORY,
    ADD_ALL_CATEGORY,
    GET_ALL_CATEGORY_BY_VENDOR_ID,
    GET_ALL_CITY,
    ADD_CITY,
    UPDATE_CITY,
    GET_SUBSCRIPTION,
    ADD_ALL_PACKAGE,
    UPDATE_PACKAGE,
    DELETE_PACKAGE,
    MAKE_PAYMENT,
    GET_MAX_VALUE_VENDOR_SUBSCRIPTION,
    GET_PAYMENT_HISTORY,
    CONFIRM_PENDING_PAYMENT,
    REJECT_PENDING_PAYMENT,
    DELETE_VENDOR_IMAGE,
    RESET_PASSWORD,
    FORGOT_PASSWORD,
    VERIFY_ONLINE_PAYMENT,
    UPDATE_CATEGORY_STATUS,
    UPDATE_CITY_STATUS,
    CREATE_EVENT,
    GET_EVENT_BY_ID,
    GET_ALL_EVENT_TEMPLATE,
    ADD_IMAGE,
    SAVE_IMAGE,
    DELETE_DESIGN_IMAGE,
    EDIT_IMAGE,
    DELETE_EVENT_TEMPLATE,
}
