import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcitymodelComponent } from './addcitymodel.component';

describe('AddcitymodelComponent', () => {
  let component: AddcitymodelComponent;
  let fixture: ComponentFixture<AddcitymodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcitymodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcitymodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
