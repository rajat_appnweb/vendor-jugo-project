import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: 'profile', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'stats', loadChildren: './stats/stats.module#StatsModule' },
            { path: 'vendor', loadChildren: './vendor/vendor.module#VendorModule' },
            { path: 'category', loadChildren: './category/category.module#CategoryModule' },
            { path: 'packages', loadChildren: './packages/packages.module#PackagesModule' },
            { path: 'payments', loadChildren: './payments/payments.module#PaymentsModule' },
            { path: 'city', loadChildren: './city/city.module#CityModule' },
            { path: 'events', loadChildren: './events/events.module#EventsModule' },
            { path: 'notification', loadChildren: './notification/notification.module#NotificationModule' },
            { path: 'view', loadChildren: './view/view.module#ViewModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
