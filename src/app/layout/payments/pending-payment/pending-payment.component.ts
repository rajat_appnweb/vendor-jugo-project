import { AppUtil } from './../../../framework/Utils/AppUtil';
import { PaymentHistoryResponse, PaymentInfo } from './../../../Model/PaymentHistoryResponse';
import { BaseComponent } from './../../../framework/BaseCompo';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../framework/common.service';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { TaskCode } from '../../../framework/globals';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pending-payment',
  templateUrl: './pending-payment.component.html',
  styleUrls: ['./pending-payment.component.scss']
})
export class PendingPaymentComponent extends BaseComponent implements OnInit {

  paymentInfo: Array<PaymentInfo>;
  status: string;

  showTable: boolean;
  constructor(public activated: ActivatedRoute, public service: CommonService, private router:Router) {
    super(service);
  }

  ngOnInit() {
    this.subscribeParams();
    this.setStatus();

  }

  subscribeParams() {

    this.activated.params.subscribe(params => {
      this.setStatus();
    });
  }


  getHeaderText() {
    if (this.activated.snapshot.params.paymentType == 1) {
      return 'Pending Payments';
    }
    if (this.activated.snapshot.params.paymentType == 2) {
      return 'Completed Payments';
    }
  }

  setStatus() {
    if (this.activated.snapshot.params.paymentType == 1) {
      this.status = 'Pending';
    }
    if (this.activated.snapshot.params.paymentType == 2) {
      this.status = 'Credit,Failed';
    }
    this.downloadData(ApiGenerator.getPaymentHistory(this.status));
  }


  confirmPayment(subTransId: number) {
    if (!AppUtil.isNullEmpty(subTransId)) {
      this.downloadData(ApiGenerator.confirmPendingPayment(subTransId));
    } else {
      alert('Cant Proceed with the Confirmation of Payment');
    }

  }


  rejectPayment(subTransId: number) {
    if (!AppUtil.isNullEmpty(subTransId)) {
      this.downloadData(ApiGenerator.rejectPendingPayment(subTransId));
    } else {
      alert('Cant Proceed with the Confirmation of Payment');
    }
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_PAYMENT_HISTORY:
          this.paymentInfo = null;
          console.log(response);
          const paymentResponse = response as PaymentHistoryResponse;
          if (!AppUtil.isNullEmpty(paymentResponse)
            && !AppUtil.isListNullOrEmpty(paymentResponse.data)) {
            this.paymentInfo = paymentResponse.data;
            this.showTable = true;
          } else {
            this.showTable = false;
          }
          break;
        case TaskCode.CONFIRM_PENDING_PAYMENT:
          this.ngOnInit();
          break;
        case TaskCode.REJECT_PENDING_PAYMENT:
          this.ngOnInit();
          break;
      }
    }
    return true;
  }


  onErrorReceived(taskCode: TaskCode, response: any) {
    switch (taskCode) {
      case TaskCode.GET_PAYMENT_HISTORY:
        console.log(response);
        alert('Could not get payment details due to some error');
        break;
      case TaskCode.CONFIRM_PENDING_PAYMENT:
        alert('Could not confirm payment due to some error');
        break;
      case TaskCode.REJECT_PENDING_PAYMENT:
        alert('Could not confirm payment due to some error');
        break;
    }
  }

  navigateProfile(profile: PaymentInfo) {
    if (!AppUtil.isNullEmpty(profile.vendor)) {
      const vendorId = profile.vendorId;
      this.router.navigate(['./profile', vendorId]);
    }
  }


}
