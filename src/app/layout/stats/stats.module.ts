import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsRoutingModule } from './stats-routing.module';
import { StatsComponent } from './stats.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NgxPaginationModule} from 'ngx-pagination';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';  
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    StatsRoutingModule,
    MatPaginatorModule,
    NgxPaginationModule,
    PaginationModule,
    FormsModule
  ],
  declarations: [StatsComponent],
  providers: [PaginationConfig]
})
export class StatsModule { 

}
