import { ActivatedRoute } from '@angular/router';
import { StorageUtil, StorageKeysEnum } from './../../framework/StorageUtil';
import { ApiGenerator } from './../../framework/ApiGenerator';
import { BaseComponent } from './../../framework/BaseCompo';
import { EventDataResponse, EventData, DesignData, DesignResponse, UserData, Images } from './../../Model/event/event';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../framework/common.service';
import { TaskCode } from '../../framework/globals';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent extends BaseComponent implements OnInit {

  popup: boolean;
  event: Array<DesignData>;
  profile: DesignData;
  eventTemplateId: string;
  image: Images
  userid: number
  headName:string;

  constructor(private service: CommonService, public activateRoute: ActivatedRoute) {
    super(service)
  }

  ngOnInit() {
    // this.getEventById();
    this.eventTemplateId = this.activateRoute.snapshot.params['eventId'];
    this.eventName();
    console.log(this.eventTemplateId);
    this.event = new Array<DesignData>();
    // this.eventData = new EventData();
    if (this.eventTemplateId) {
      this.getEventById();
    }

  }
  // getImageById() {
  //   // this.profile.designId = this.profile.parentEventTemplateId
  //   // console.log("id is "+ this.profile.designId);
  //   this.profile.parentEventTemplateId = +this.eventTemplateId;
  //   this.downloadData(ApiGenerator.getEventById(this.eventTemplateId))
  //   // this.downloadData(ApiGenerator.getEventById(this.eventTemplateId));
  // }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.GET_EVENT_BY_ID:
          const des = response as DesignResponse;
          this.event = des.data;

          console.log(this.event)
          break;
        case TaskCode.DELETE_DESIGN_IMAGE:
          this.getEventById()
          // console.log("hello "+);
          

          break;
      }
    }

    return true;
  }

  getEventById() {
    this.downloadData(ApiGenerator.getEventById(this.eventTemplateId));
  }

  onSelect(item: DesignData) {
    this.popup = true;
    this.profile = item;
  }

  onUpdate(event) {
    this.popup = false;
    this.getEventById();
  }

  onAdd() {
    this.popup = true;
    this.image = null;
  }
  onDeleteClicked(photo) {
    let isOk = confirm("Are you sure want to delete this image");
    if (isOk) {
      this.downloadData(ApiGenerator.deleteImage(photo));
    }
  }
eventName(){
  switch(this.eventTemplateId){
    case '1': this.headName='Birthday'
    break;
    case '2': this.headName='Marriage Aniversary'
    break;
    case '3': this.headName='Ring Ceremony'
    break;
    case '4': this.headName='	Wedding'
    break;
    case '5': this.headName='Success'
    break;
    case '6': this.headName='General'
    break;
    case '7': this.headName='Festivals'
    break;
    case '8': this.headName='Special Days'
    break;
    case '9': this.headName='Bbday'
    break;
  }
}

}