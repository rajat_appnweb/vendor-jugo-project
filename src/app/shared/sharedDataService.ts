import { Vendor } from './../Model/Loginrequest/LoginRequest';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';


@Injectable()

export class CommonDataService {

    // firstName : string;
    // lastName : string;
    vendorFullName : string;
    
    constructor(private http: Http) {
    }

    setVendorName(vendor : Vendor) {
        // this.firstName = firstName;
        // this.lastName = lastName;
        return this.vendorFullName = vendor.firstName+" "+vendor.lastName;
     }
     getVendorName(){
        //  return this.firstName+" "+this.lastName;
     }
}
