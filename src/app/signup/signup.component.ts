/// <reference types="@types/googlemaps" />
import { AppUtil } from './../framework/Utils/AppUtil';
import { Login, Vendor, LoginResponse } from './../Model/Loginrequest/LoginRequest';
import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { routerTransition } from '../router.animations';
import { RegisterResponse, RegisterRequest, Categories } from '../Model/registration/RegisterRequest';
import { BaseResponse } from '../framework/BaseResponseModel';
import { CommonService } from '../framework/common.service';
import { BaseComponent } from '../framework/BaseCompo';
import { ApiGenerator } from '../framework/ApiGenerator';
import { TaskCode } from '../framework/globals';
import { Router } from '@angular/router';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
// import { map, startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { VendorCategory, Category, CategoryResponse } from '../Model/vendor/vendor';
import { CityData, CityResponse } from '../Model/city/city';
// import { element } from 'protractor';
import { MapsAPILoader } from '@agm/core';
// import { } from 'googlemaps';
import { Http } from '@angular/http';
// import { takeUntil } from 'rxjs/operators';
import 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  animations: [routerTransition()]
})
export class SignupComponent extends BaseComponent implements OnInit {
  registerRequest: RegisterRequest;
  copy: RegisterRequest;
  pincode: string;
  addressline1: string;
  addressline2: string;
  locality: string;
  landmark: string;
  city: string;
  message: string;
  message2: string;
  categories: Category[] = [];
  categoriesId: number[] = [];
  vendorCat: VendorCategory;
  categoriesdata: Array<Category>;
  cityData: Array<CityData>;
  selectedvalue: string;
  categoryvalue: Category;
  userid: string;
  confirmpassword: string;
  confirmmsg: string;
  vendorid: Number;
  categoryid: Categories;
  login: Login;
  vendor: Vendor;
  vendorFullName: string;
  selectedCityId: number;
  venderAddress:string;
  mapPlaces: any;
  mapCurrentAddress = '';
  public latitude: number;
  public longitude: number;
  public zoom = 4;

  @ViewChild('addSearch')
  public searchElementRef: ElementRef;



  @ViewChild('categoryInput') categoryInput: ElementRef;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  separatorKeysCodes = [ENTER, COMMA];
  categoryCtrl = new FormControl();
  filteredCategory: Observable<any[]>;
  registerdata: string



  constructor(private router: Router, public service: CommonService, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private http: Http) {
    super(service);

  }

  ngOnInit() {
    this.registerRequest = new RegisterRequest();
    this.login = new Login();
    this.selectedCityId = 0;
    this.registerRequest.categories = new Categories();
    this.registerRequest.categories.cetegoryId = new Array<number>();
    this.copy = new RegisterRequest();
    this.categoriesdata = new Array<Category>();
    this.cityData = new Array<CityData>();
    this.getAllCategory();
    this.getCityApi();
    this.mapsAPILoader.load().then(() => {
      this.mapAutoComplete();
    });
  }

  mapAutoComplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      // types: ['address']
    });
    autocomplete.addListener('place_changed', () => {
      this.ngZone.run(() => {
        // get the place result
        const place: google.maps.places.PlaceResult = autocomplete.getPlace();

        // verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        // set latitude, longitude and zoom
        this.latitude = place.geometry.location.lat();
        this.longitude = place.geometry.location.lng();
        this.zoom = 16;
        this.getAddressByLatLong();
        // this.mapPlaces = place;
      });
    });
  }

  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 16;
        this.getAddressByLatLong();
      });
    }
  }
  getAddressByLatLong() {
    this.http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.latitude + ',' + this.longitude+ '&API_KEY=' + 'AIzaSyBblTka0hxpRRDaLsLemtmtbPyp0j5FeAI')
      .map(res => res.json())
      .subscribe(
        res => {
          console.log(res)
          res.status !== 'OK' ? console.log(res.message) : this.mapPlaces = res.results;
          console.log(this.mapPlaces);
        },
        error => { alert(error); },
        () => {
          if(this.mapPlaces !=null && this.mapPlaces.length>0){
            this.mapCurrentAddress = this.mapPlaces[0].formatted_address;
          }
        }
      );
      // this.saveBasicInfo();
  }


  hitRegisterApi(req: RegisterRequest) {
    const http = ApiGenerator.postRegisterRequest(req);
    this.downloadData(http);

  }



  onCitySelect(){

    console.log(this.selectedCityId);
    
    let index =  this.cityData.findIndex(element =>
        element.id == this.selectedCityId
      )

      this.cityData[index].id;
     console.log(this.cityData[index].id);
     
      
  }


  moveToHomepage() {
    this.router.navigate(['/makepayment']);
  }


  onResponseReceived(taskCode: TaskCode, response: any) {

    const isSuccess = super.onResponseReceived(taskCode, response);
    if (isSuccess) {
      switch (taskCode) {
        case TaskCode.SIGN_UP:
          const res = response;
          console.log(res);
          if (res.error == false) {
            this.router.navigate(['login']);
          }
          if (res.error == true) {
            alert(res.message);
          }
          // this.onRegisterResponseReceived(res);
          // console.log(res.data);
          break;
        case TaskCode.GET_ALL_CATEGORY:
          const categoriesResponse = response as CategoryResponse;
          this.categoriesdata = categoriesResponse.data;
          console.log(this.categoriesdata);
          break;
          case TaskCode.GET_ALL_CITY:
          const cityResponse = response as CityResponse;
          this.cityData = cityResponse.data;
          console.log(this.cityData);
          break;


        case TaskCode.ADD_CATEGORY:
          console.log(response);
          // const cat = response as CategoryResponse;
          // this.categoriesdata = cat.data;
          console.log(this.categoriesdata)
          break;
        case TaskCode.LOGIN:
          console.log(response);
          const loginResponse = response as LoginResponse;
          this.vendor = loginResponse.data;
          localStorage.setItem("vendorId", this.vendor.id.toString());
          this.vendorFullName = this.vendor.firstName + " " + this.vendor.lastName;
          localStorage.setItem("vendorFullName", this.vendorFullName);
          localStorage.setItem("role", this.vendor.role);// only use for admin 
          localStorage.setItem("isLoggedin", "true");
          // this.router.navigate(["/"]);
          this.moveToHomepage();

          break;
      }
    }

    return true;
  }



  onErrorReceived(taskCode: TaskCode, response: any) {
    switch (taskCode) {
      case TaskCode.SIGN_UP:
        alert(response.message);
        break;

    }
  }





  onRegisterResponseReceived(rRes: RegisterResponse) {
    if (!rRes.error) {
      this.onlogin(this.registerRequest);


    }
    else {
      this.message = rRes.message;
      alert(this.message);
      // this.message = "This email Address is already exist"
    }
    // console.log(rRes);
  }

  remove(category: any): void {
    console.log(category);
    const index = this.categories.indexOf(category);
    this.categories.splice(index, 1);
  }

  filter(name: string) {
    console.log(this.categories)
    return this.categories.filter(ele =>
      ele.categoryName.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    console.log(event);

    if (this.categories.indexOf(event.option.value) == -1) {
      this.categories.push(event.option.value);
    }

    this.categoryInput.nativeElement.value = '';
    this.categoryCtrl.setValue(null);

  }


  getCityApi(){
    this.downloadData(ApiGenerator.getAllCity());
  }

  getAllCategory() {
    this.downloadData(ApiGenerator.getAllCategory())
  }

  passwordconfirm() {
    if (this.registerRequest.password !== this.confirmpassword && this.registerRequest.password.length !== this.confirmpassword.length) {
      this.confirmmsg = "please check password"

    }
    else {
      this.confirmmsg = "";
    }
  }



  onRegister() {

    if(this.selectedCityId == 0){
      alert("Please select city");
      return false;
    }else{
      this.registerRequest.cityId = this.selectedCityId;
      this.city = this.cityData.find(element =>
      element.id == this.selectedCityId ).cityName;
  
      this.pincode = this.cityData.find(element =>
        element.id == this.selectedCityId ).pincode;
    }

    this.registerRequest.address = this.addressline1 + "," + this.addressline2 + "," + this.landmark + "," + this.locality + "," + this.city + "," + this.pincode;
    // this.moveToHomepage();
    console.log(this.registerRequest.address);

    if (AppUtil.isNullEmpty(this.registerRequest.firstName)) {

      console.log("enter all value");
      alert("please enter your name ")
    }
    if (this.registerRequest.isValid) {
      // console.log()
      if (this.isValid()) {
        console.log("Request :: " + JSON.stringify(this.registerRequest));

        this.downloadData(ApiGenerator.postRegisterRequest(this.registerRequest));
        console.log(this.registerRequest)
      }
      console.log(this.registerRequest.address);
    }
    
    else {
      
      console.log(this.city);
    console.log(this.selectedCityId);
    }

  }


  isValid() {
    if (AppUtil.isListNullOrEmpty(this.categories)) {
      alert("Please select atleast 1 category to register");
      return false;
    } else {
      this.registerRequest.categories = new Categories();
      this.registerRequest.categories.cetegoryId = [];
      this.categories.forEach(c => {
        this.registerRequest.categories.cetegoryId.push(c.categoryId);
      })
      return true;
    }
  }

  onlogin(item: RegisterRequest) {
    this.login.username = item.username;
    this.login.password = item.password;
    this.downloadData(ApiGenerator.postLogin(this.login));


  }


}